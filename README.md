# Docker Image for Compiling Windows Qt apps
Build Status: [![build status](https://gitlab.com/luz_booyadude/docker-mxe/badges/master/build.svg)](https://gitlab.com/luz_booyadude/docker-mxe/commits/master)

This docker image was built specifically to statically compile Qt apps for Windows. Current Qt11.1 is compiled in this docker image and will generate 32bit Qt apps.
## Instruction
1. Use this link to fetch the docker: `registry.gitlab.com/luz_booyadude/docker-mxe:latest`
2. In .yml, under script, add;

    a. `export PATH=/mxe/usr/bin:$PATH`
    
    b. `/mxe/usr/bin/i686-w64-mingw32.static-qmake-qt5`
    
    c. `make`
3. The artifacts will generated in `release` folder.

