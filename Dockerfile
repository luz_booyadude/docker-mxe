FROM debian:latest
MAINTAINER Zulhilmi Ramli <soul@outlook.my>

RUN apt-get update \
    && apt-get install -y \
    autoconf \
    automake \
    autopoint \
    bash \
    bison \
    bzip2 \
    curl \
    flex \
    g++ \
    g++-multilib \
    gettext \
    git \
    gperf \
    intltool \
    libc6-dev-i386 \
    libgdk-pixbuf2.0-dev \
    libltdl-dev \
    libssl-dev \
    libtool-bin \
    libxml-parser-perl \
    make \
    openssl \
    p7zip-full \
    patch \
    perl \
    pkg-config \
    python \
    ruby \
    scons \
    sed \
    unzip \
    wget \
    xz-utils \
    g++-multilib \
    libc6-dev-i386 \
    && apt-get clean

ENV MXE-VERSION=2017-02-11
RUN (git clone https://github.com/mxe/mxe.git \
    && cd mxe && make qtbase)

CMD ["/bin/bash"]
